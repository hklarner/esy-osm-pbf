# esy-osm-pbf

`esy.osm.pbf` is a low-level Python library to interact with
[OpenStreetMap](https://www.openstreetmap.org) data files in the [Protocol
Buffers (PBF)](https://developers.google.com/protocol-buffers/) format.

## Usage

To count the amount of parks in the OpenStreetMap Andorra `.pbf` file (at least
according to a copy from [geofabrik](https://www.geofabrik.de/)), do this:

First download a copy of the andorra dataset:

```python
>>> import os, urllib.request
>>> if not os.path.exists('andorra.osm.pbf'):
...     filename, headers = urllib.request.urlretrieve(
...         'https://download.geofabrik.de/europe/andorra-190101.osm.pbf',
...         filename='andorra.osm.pbf'
...     )

```

Open the file and iterate over all entry and count those with a tag `leisure`
having a value of `park`.

```python
>>> import esy.osm.pbf
>>> osm = esy.osm.pbf.File('andorra.osm.pbf')
>>> len([entry for entry in osm if entry.tags.get('leisure') == 'park'])
21

```

For more details, jump to the
[documentation](https://dlr-ve-esy.gitlab.io/esy-osm-pbf).